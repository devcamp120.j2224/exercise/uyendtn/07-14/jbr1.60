public class Date {
    int date;
    int month;
    int year;

    public int getDate() {
        return date;
    }
    public void setDate(int date) {
        this.date = date;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public Date() {
    }
    public Date(int date, int month, int year) {
        this.date = date;
        this.month = month;
        this.year = year;
    }

    public String toString() {
        String dateFormat = new String();
        if (date < 10 && month < 10){
            dateFormat = "0" + date + "/0" + month + "/" + year;
            return dateFormat;
        } else if (date >= 10 && month < 10) {
            dateFormat =  date + "/0" + month + "/" + year;
            return dateFormat;
        } else if (date >= 10 && month >= 10) {
            dateFormat =  date + "/" + month + "/" + year;
            return dateFormat;
        }
        return dateFormat;
    }
}
